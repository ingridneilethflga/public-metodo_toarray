/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.lang.reflect.Array;

/**
 * Clase que modela una lista simple enlazada genérica
 *
 * @author madarme
 */
public class ListaS<T> {

    private Nodo<T> cabeza = null; //Es redundante--> por aclaración
    private int tamanio;

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }

    public void insertarInicio(T info) {
        Nodo<T> nuevo = new Nodo(info, this.cabeza); //Creación (new)
        this.cabeza = nuevo;
        this.tamanio++;

    }

    public void insertarFin(T info) {

    }

    public void borrarTodo() {
        this.tamanio = 0;
        this.cabeza = null;
    }

    @Override
    public String toString() {
        String msg = "Cabeza->";
        //referenciación: Nodo<T> inicio=this.cabeza
        for (Nodo<T> inicio = this.cabeza; inicio != null; inicio = inicio.getSig()) {
            //inicio.getInfo()--> es T (String, Persona, Monomio...)
            msg += inicio.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    public boolean esVacia() {
        return this.cabeza == null; //this.tamanio==0
    }

    public T[] toArray() {
        if (this.esVacia()) {
            throw new RuntimeException("No se puede crear convertir la lista a un vector, por que está es vacía");
        }

        T[] vector = (T[]) new Object[this.tamanio];  //Creo un arreglo de objetos
        int i = 0;
        T[] genericArray = null; //Creo un arreglo generico

        for (Nodo<T> inicio = (Nodo<T>) this.cabeza; inicio != null; inicio = inicio.getSig()) {
            //Añado todos los elementos al arreglo de objetos
            if (i == 0) {//Necesito que sólo esto ocurra una vez y obtener la clase que usa el vector[] (Vaca, Persona, Integer, String, etc.)
                vector[i] = inicio.getInfo();
                // Y así completar mi arreglo generico de dicha clase obtenida
                genericArray = (T[]) Array.newInstance(vector[i].getClass(), this.tamanio);
            }
            //Añado los elementos al arreglo generico
            genericArray[i] = inicio.getInfo();
            i++;
        }
        return genericArray;
    }
    
   public Object[] toArray2() {
        if (this.esVacia()) {
            throw new RuntimeException("No se puede crear convertir la lista a un vector, por que está es vacía");
        }
        Object temp[] = new Object[this.tamanio];

        int i = 0;
        for (Nodo<T> inicio = this.cabeza; inicio != null; inicio = inicio.getSig()) {
            temp[i] = inicio.getInfo();
            i++;
        }
        return (temp);
    }
}
