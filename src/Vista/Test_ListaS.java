/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;
import Modelo.Vaca;
import ufps.util.colecciones_seed.ListaS;

/**
 * Clase para probar los métodos de una lista simple enlazada
 *
 * @author madarme
 */
public class Test_ListaS {

    public static void main(String[] args) throws Exception {
        ListaS<String> l1 = new ListaS();
        ListaS<Integer> l2 = new ListaS();
        ListaS<Vaca> potrero = new ListaS();
        ListaS<Persona> dPersona = new ListaS();
        /*
            Crear vacas
         */
        potrero.insertarInicio(new Vaca("Dulcinea", 200F));
        potrero.insertarInicio(new Vaca("Pepa", 300F));
        potrero.insertarInicio(new Vaca("Lucinda", 100F));


        /*
            Imprime potrero.toString:
            Cabeza-->
                    (lucinda, 100)-(pepa,300)-(dulcinea,200)-->NULL
         */
        System.out.println("Mi lista es:\n" + potrero.toString());
        System.out.println("Esto es la lista en un vector: ");
        Vaca vector[] = potrero.toArray();
        for (Vaca v : vector) {
            System.out.println(v.toString());
        }

        //Probar con otra clase (Persona)
        dPersona.insertarInicio(new Persona("William", (short) 30, (short) 10, (short) 1981, (short) 14, (short) 32, (short) 40));
        dPersona.insertarInicio(new Persona("Ximena", (short) 25, (short) 03, (short) 1981, (short) 20, (short) 45, (short) 26));
        dPersona.insertarInicio(new Persona("Zolivan", (short) 28, (short) 02, (short) 1977, (short) 8, (short) 44, (short) 55));

        System.out.println("Mi persona es:\n" + dPersona.toString());
//
        System.out.println("Esto es la lista en un vector Persona: ");
        Persona vector2[] = dPersona.toArray();
        for (Persona p : vector2) {
            System.out.println(p.toString());
        }
        l1.insertarInicio("Luis");
        l1.insertarInicio("Anderson");
        l1.insertarInicio("Edna");
        l1.insertarInicio("Jaider");

        System.out.println("Mi lista es:\n" + l1.toString());

        /*
            Imprimo l1:
            Jaider-Edna-Anderson-Luis
         */
        l1.insertarFin("Madarme");
        l1.insertarFin("Jenifer");
        l1.insertarFin("Juan P");

        /*
            Imprimo L1:
            cabeza-->
                    Jaider-Edna-Anderson-Luis-Madarme-Jenifer-Juan P --> NULL
        
         */
        l1.insertarInicio("luis");
        l1.insertarInicio("anderson"); //l1.gettamanio= 2
        
        System.out.println("Esto es la lista en un vector L1: ");
        String vector3[] = l1.toArray();
        for (String s : vector3) {
            System.out.println(s.toString());
        }

    }

}
